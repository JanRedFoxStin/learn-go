package controllers

import (
	"net/http"

	"ib/goapp/controllers/product"
	"ib/goapp/controllers/web"

	"github.com/gorilla/mux"
)

// New .
func New() http.Handler {
	r := mux.NewRouter()

	// website
	r.HandleFunc("/", web.Root).Methods("GET")

	// product
	// r.HandleFunc("/product", product.New).Methods("POST")
	r.HandleFunc("/product/{id}", product.Get).Methods("GET")

	return r
}
