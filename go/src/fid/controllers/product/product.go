package product

import (
	"fmt"
	"net/http"
	"strconv"

	"ib/goapp/db/mysql"
	"ib/goapp/models/product"

	"github.com/gorilla/mux"
)

func Get(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idParam := vars["id"]

	id, err := strconv.Atoi(idParam)
	if err != nil {
		fmt.Fprintf(w, "Ошибка преобразование типов")
		return
	}

	product.ProductModel, err := mysql.InitializeDb()

	p, err := ProductModel.Get(id)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	fmt.Fprintf(w, "Get product page "+idParam)
	fmt.Println(p)
}
