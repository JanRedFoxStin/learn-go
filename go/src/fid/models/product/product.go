package product

import (
	"database/sql"
	"errors"
	"ib/goapp/db/mysql"
	"time"
)

// var (
// 	ErrInvalidName = errors.New("invalid product name")
// 	ErrInvalidID   = errors.New("invalid product id")
// 	ErrInvalidJSON = errors.New("invalid product json")
// )

// Product .
type Product struct {
	ID         int
	Name       string
	Content    string
	CreateTime time.Time
}

type ProductModel struct {
	mysql.SqlDB
}

func (m *ProductModel) Get(id int) (*Product, error) {
	stmt := `SELECT id, name, content, createtime FROM products WHERE id = ?`

	row := m.DB.QueryRow(stmt, id)

	var product *Product

	err := row.Scan(&product.ID, &product.Name, &product.Content, &product.CreateTime)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, errors.New("models: подходящей записи не найдено")
		} else {
			return nil, err
		}
	}

	// Если все хорошо, возвращается объект Product.
	return product, nil
}

// func (p *Product) String() string {
// 	b, err := json.Marshal(p)
// 	if err != nil {
// 		err = ErrInvalidJSON
// 		log.Fatal("ERR:", err)
// 		return err.Error()
// 	}

// 	return string(b)
// }

// New .
// func New(name string) (product *Product, err error) {
// 	log.Fatal("name:", name)

// 	// verify product name
// 	if name == "" {
// 		err = ErrInvalidName
// 		log.Fatal("ERR:", err)
// 		return
// 	}

// 	// create new product
// 	product = &Product{
// 		Name:       name,
// 		CreateTime: time.Now(),
// 	}

// 	// save product into db
// 	collection := mongodb.DB.C("product")
// 	err = collection.Insert(product)
// 	if err != nil {
// 		log.Fatal("ERR collection.Insert:", err)
// 		return
// 	}

// 	log.Fatal("product:", product)
// 	return
// }

// Get .
// func Get(id string) (product *Product, err error) {
// log.Fatal("id:", id)

// // verify product id
// if !bson.IsObjectIdHex(id) {
// 	err = ErrInvalidID
// 	log.Fatal("ERR:", err)
// 	return
// }

// // get product by id
// product = &Product{}
// collection := mongodb.DB.C("product")
// err = collection.FindId(bson.ObjectIdHex(id)).One(product)
// if err != nil {
// 	if err == mgo.ErrNotFound {
// 		err = nil
// 		product = nil
// 		log.Fatal("not found")
// 		return
// 	}

// 	log.Fatal("ERR collection.FindId:", err)
// 	return
// }

// log.Fatal("product:", product)
// return
// }
