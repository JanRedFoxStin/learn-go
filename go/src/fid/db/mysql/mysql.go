package mysql

import (
	"database/sql"
	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	log "github.com/sirupsen/logrus"
)

type SqlDB struct {
	DB *sql.DB
}

func InitializeDb() (*sql.DB, error) {
	// var batabase SqlDB
	var batabase SqlDB
	var err error
	DbUser := getEnv("MYSQL_USER", "root")
	DbPassword := getEnv("MYSQL_PASSWORD", "root")
	DbHost := getEnv("MYSQL_URL", "db")
	DbPort := getEnv("MYSQL_PORT", "3308")
	DbName := getEnv("MYSQL_DATABASE", "digitalprofile")
	DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)

	batabase.DB, err = sql.Open(getEnv("DB_DRIVER", "mysql"), DBURL)
	if err != nil {
		log.Error("error connecting to mysql database", err)
		return nil, err
	}
	defer batabase.DB.Close()
	if err = batabase.waitToConnect(); err != nil {
		return nil, err
	}

	log.Info("connection to mysql successful")
	return batabase.DB, nil
}

func getEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		fmt.Println(key)
		fmt.Println("default value using")
		return defaultValue
	}
	return value
}

func (db *SqlDB) waitToConnect() error {
	var err error
	for i := 0; i < 60; i++ {
		err = db.DB.Ping()
		if err == nil {
			return nil
		}
		time.Sleep(time.Second)
	}
	return err
}
