package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"ib/goapp/controllers"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
)

const (
	defaultPort = "8080"

	idleTimeout       = 30 * time.Second
	writeTimeout      = 180 * time.Second
	readHeaderTimeout = 10 * time.Second
	readTimeout       = 10 * time.Second
)

type sqlDB struct {
	DB *sql.DB
}

func main() {
	initialize()
}

func initialize() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("error loading .env file", err)
	}

	handler := controllers.New()

	server := &http.Server{
		Addr:    "0.0.0.0:" + defaultPort,
		Handler: handler,

		IdleTimeout:       idleTimeout,
		WriteTimeout:      writeTimeout,
		ReadHeaderTimeout: readHeaderTimeout,
		ReadTimeout:       readTimeout,
	}

	err := server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}
