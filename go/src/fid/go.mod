module ib/goapp

go 1.14

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20221017161538-93cebf72946b // indirect
	github.com/go-sql-driver/mysql v1.7.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.5.1
	github.com/oakmound/oak/v4 v4.1.0 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.26.0 // indirect
	github.com/sirupsen/logrus v1.9.0
	golang.org/x/exp v0.0.0-20230425010034-47ecfdc1ba53 // indirect
	golang.org/x/exp/shiny v0.0.0-20230425010034-47ecfdc1ba53 // indirect
	golang.org/x/image v0.7.0 // indirect
	golang.org/x/mobile v0.0.0-20230427221453-e8d11dd0ba41 // indirect
	golang.org/x/sys v0.7.0 // indirect
	gopkg.in/redis.v5 v5.2.9
)
